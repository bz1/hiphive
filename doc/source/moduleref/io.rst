Input/output and logging
========================

The functions and modules described below provide functionality for controlling
the verbosity of the output as well as for reading and writing force constant
matrices in different formats.

Input and output
----------------

.. index::
   single: Function reference; Input/output

.. automodule:: hiphive.input_output
   :members:
   :undoc-members:
   :noindex:

.. index::
   single: Function reference; Input/output GPUMD

.. automodule:: hiphive.input_output.gpumd
   :members:
   :undoc-members:
   :noindex:

.. index::
   single: Function reference; Logging

Logging
--------

.. autofunction:: hiphive.input_output.logging_tools.set_config
   :noindex:
